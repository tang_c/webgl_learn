import { Renderer } from "./renderer2D";

let vertexShaderSource =
    "precision mediump float;\n" +
    "attribute vec4 a_Position;\n" +
    "varying vec4 v_Position;\n" +
    "void main() {\n" +
    "  gl_Position = a_Position;\n" +
    "  v_Position = a_Position;\n" +
    "}\n";
let fragmentShaderSource =
    `
    precision mediump float;
    varying vec4 v_Position;
    uniform float time;
    vec4 HSV2RGBA(float h,float s,float v) {
        float r, g, b;
        if (s == 0.0) {
            r = g = b = v;
        }
        else {
            if (v == 0.0) {
                r = g = b = 0.0;
            }
            else {
                if (h == 1.0) h = 0.0;
                h *= 6.0;
                s = s;
                v = v;
                float i = floor(h);
                float f = h - i;
                float p = v * (1.0 - s);
                float q = v * (1.0 - (s * f));
                float t = v * (1.0 - (s * (1.0 - f)));
                if(i == 0.0){r = v;g = t;b = p;}
                else if(i == 1.0){r = q;g = v;b = p;}
                else if(i == 2.0){r = p;g = v;b = t;}
                else if(i == 3.0){r = p;g = q;b = v;}
                else if(i == 4.0){r = t;g = p;b = v;}
                else if(i == 5.0){r = v;g = p;b = q;}
            }
        }
        return vec4(r,g,b,1.0);
    }

    float PI = 3.1415926;
    void main() {
    float angle = atan(v_Position.x,v_Position.y) / PI * 180.0 + 180.0 ;
    angle += time * 100.0;
    float h = (angle ) / 360.0;
    if(h > 1.0){
        h = h - 1.0;
    }else if(h < 0.0){
        h = h + 1.0;
    }
    vec4 color = HSV2RGBA(fract(h), 1.0 , 1.0);
        gl_FragColor = color;
    }

`

let canvas = <any>document.querySelector("#canvas");
if (!canvas) {
    console.error('没有canvas');
}
console.log('canvas', canvas)
canvas.setAttribute('width', "600px");
canvas.setAttribute('height', "600px");
let gl = <WebGLRenderingContext>canvas.getContext('webgl');
// window['gl'] = gl;
if (!gl) {
    throw ('没有gl')
}

let rectProgram = Renderer.createProgram(gl, vertexShaderSource, fragmentShaderSource);
if (rectProgram) {
    gl.useProgram(rectProgram);
}



