"use strict";
/* 基于webgl图形库的渲染类 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Renderer = void 0;
var Renderer = /** @class */ (function () {
    function Renderer() {
    }
    Renderer.createProgram = function (gl, vertexShaderSource, fragmentShaderSource) {
        var program = gl.createProgram();
        var vertexShader = Renderer.createShader(gl, vertexShaderSource, gl.VERTEX_SHADER);
        var fragmentShader = Renderer.createShader(gl, fragmentShaderSource, gl.FRAGMENT_SHADER);
        gl.attachShader(program, vertexShader);
        gl.attachShader(program, fragmentShader);
        gl.linkProgram(program);
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            var info = gl.getProgramInfoLog(program);
            console.error("Could not link WebGL program. \n\n" + info);
            gl.deleteProgram(program);
            return null;
        }
        gl.deleteShader(fragmentShader);
        gl.deleteShader(vertexShader);
        return program;
    };
    Renderer.createShader = function (gl, sourceCode, type) {
        var shader = gl.createShader(type);
        gl.shaderSource(shader, sourceCode);
        gl.compileShader(shader);
        // console.log(gl.getShaderParameter(shader, gl.COMPILE_STATUS))
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            var info = gl.getShaderInfoLog(shader);
            console.error("Could not compile WebGL program. \n\n" + info);
            gl.deleteShader(shader);
        }
        return shader;
    };
    return Renderer;
}());
exports.Renderer = Renderer;
