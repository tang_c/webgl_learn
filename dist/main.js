"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var renderer2D_1 = require("./renderer2D");
var vertexShaderSource = "precision mediump float;\n" +
    "attribute vec4 a_Position;\n" +
    "varying vec4 v_Position;\n" +
    "void main() {\n" +
    "  gl_Position = a_Position;\n" +
    "  v_Position = a_Position;\n" +
    "}\n";
var fragmentShaderSource = "\n    precision mediump float;\n    varying vec4 v_Position;\n    uniform float time;\n    vec4 HSV2RGBA(float h,float s,float v) {\n        float r, g, b;\n        if (s == 0.0) {\n            r = g = b = v;\n        }\n        else {\n            if (v == 0.0) {\n                r = g = b = 0.0;\n            }\n            else {\n                if (h == 1.0) h = 0.0;\n                h *= 6.0;\n                s = s;\n                v = v;\n                float i = floor(h);\n                float f = h - i;\n                float p = v * (1.0 - s);\n                float q = v * (1.0 - (s * f));\n                float t = v * (1.0 - (s * (1.0 - f)));\n                if(i == 0.0){r = v;g = t;b = p;}\n                else if(i == 1.0){r = q;g = v;b = p;}\n                else if(i == 2.0){r = p;g = v;b = t;}\n                else if(i == 3.0){r = p;g = q;b = v;}\n                else if(i == 4.0){r = t;g = p;b = v;}\n                else if(i == 5.0){r = v;g = p;b = q;}\n            }\n        }\n        return vec4(r,g,b,1.0);\n    }\n\n    float PI = 3.1415926;\n    void main() {\n    float angle = atan(v_Position.x,v_Position.y) / PI * 180.0 + 180.0 ;\n    angle += time * 100.0;\n    float h = (angle ) / 360.0;\n    if(h > 1.0){\n        h = h - 1.0;\n    }else if(h < 0.0){\n        h = h + 1.0;\n    }\n    vec4 color = HSV2RGBA(fract(h), 1.0 , 1.0);\n        gl_FragColor = color;\n    }\n\n";
var canvas = document.querySelector("#canvas");
if (!canvas) {
    console.error('没有canvas');
}
console.log('canvas', canvas);
canvas.setAttribute('width', "600px");
canvas.setAttribute('height', "600px");
var gl = canvas.getContext('webgl');
// window['gl'] = gl;
if (!gl) {
    throw ('没有gl');
}
var rectProgram = renderer2D_1.Renderer.createProgram(gl, vertexShaderSource, fragmentShaderSource);
if (rectProgram) {
    gl.useProgram(rectProgram);
}
